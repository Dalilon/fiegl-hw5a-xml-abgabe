
public class Main {

	public static void main(String[] args) {
		int[] fibonnaci = generiereFibonnaci(30);
		ausgeben(fibonnaci);

	}

	static int[] generiereFibonnaci(int anzahl){
		int[] liste = new int[anzahl];
		
		if(anzahl < 3){
			System.out.println("Zu kurz!");
		}
		else{
			
			liste[0]=1;
			liste[1]=2;
			
		for(int i = 2 ; i < anzahl ; i++){
			liste[i] = liste[i-1] + liste[i-2];
		}
		}
		return liste;
				}
	
	static void ausgeben(int[] liste){
		for(int i = 0; i< liste.length ; i ++){
			System.out.println(liste[i]);
		}
	}
	
}
