import java.io.IOException;

public class Main {

	// Zusammen mit Herrn Kanetscheider gemacht
	
	public static void main(String[] args) {
		String csv = "src/Daten/shischa.csv";
		String xml = "src/Daten/data.xml";
		CSV_to_XML_Formatter xmlFormatter = new CSV_to_XML_Formatter(csv, xml);
		try {
			xmlFormatter.writeToXML();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
