import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Main {
	
	public static void main(String[] args) throws FileNotFoundException, ParserConfigurationException, TransformerException {
		
		String csv = "/CSV.csv";
		String xml = "/XML.xml";
		
		
		ArrayList<String> liste = new ArrayList<>();
				
		Scanner s = new Scanner(new File(csv));
		while (s.hasNextLine()) {
			liste.add(s.nextLine());
		}
		s.close();
		
		
		DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dokumentBuilder = documentFactory.newDocumentBuilder();
		
		Document document = (Document) dokumentBuilder.newDocument();
		Element rootKnoten = document.createElement("ShischaLounge");
		document.appendChild(rootKnoten);
		
		for(int i = 0 ; i < liste.size() ; i++){
		String[] listen = liste.get(i).split(";");
		String tisch = listen[0];
		String shisha = listen[1];
		String tabak = listen[2];
		String preis = listen[3];
		
		Element neuerTisch = document.createElement("Tisch");
		
		Element tischNr = document.createElement("TischNr");
		tischNr.appendChild(document.createTextNode(tisch));
		neuerTisch.appendChild(tischNr);
		
		Element shischaKnoten = document.createElement("Shisha");
		shischaKnoten.appendChild(document.createTextNode(shisha));
		neuerTisch.appendChild(shischaKnoten);
		
		Element tabakKnoten = document.createElement("Tabak");
		tabakKnoten.appendChild(document.createTextNode(tabak));
		neuerTisch.appendChild(tabakKnoten);
		
		Element preisKnoten = document.createElement("Preis");
		preisKnoten.appendChild(document.createTextNode(preis));
		neuerTisch.appendChild(preisKnoten);
		
		rootKnoten.appendChild(neuerTisch);
		
		}
		
	
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(document);
		StreamResult result = new StreamResult(new File(xml));
		transformer.transform(source, result);

	}

}

