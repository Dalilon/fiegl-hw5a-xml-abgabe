<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h2>XSLT Hausuebung - Fiegl Marcel 5AHWI</h2>
    <table border="1">
      <tr bgcolor="#9acd32">
        <th style="text-align:left">Tisch</th>
        <th style="text-align:left">Shisha</th>
	<th style="text-align:left">Tabak</th>
	<th style="text-align:left">Preis</th>
      </tr>
      <xsl:for-each select="ShishaLounge/Tisch">
      <tr>
        <td><xsl:value-of select="TischNr"/></td>
        <td><xsl:value-of select="Shisha"/></td>
	<td><xsl:value-of select="Tabak"/></td>
	<td><xsl:value-of select="Preis"/></td>
      </tr>
      </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>

